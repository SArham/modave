from src.parser.argument_parser import InputArgumentParser
from src.dataset_generator.dataset_generator import DatasetGenerator
from src.model_result_parser.model_verification_parser import ModelDataParser
from src.data_verification.data_verifier import DataVerifier


def get_dataset_from_file(parser):
    parser.extract_dataset()
    dataset, dataset_mapping, model_mapping = parser.get_dataset()
    return dataset, dataset_mapping, model_mapping


def get_dataset_generator(inputdata=None):
    parser = InputArgumentParser(inputdata)
    return DatasetGenerator(*get_dataset_from_file(parser))


def get_model_data_converter(model_size):
    return ModelDataParser(model_size=model_size)


def get_data_verifier():
    return DataVerifier()

def get_results():
    pass