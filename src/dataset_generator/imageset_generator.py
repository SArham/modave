import os
import cv2
import json

from .base_generator import BaseGenerator
from src.annotation_parser.format_selector import FormatSelector

"""
    [location, annt_location, format, annt_format, is_image]
"""


class ImageGenerator(BaseGenerator):

    def __init__(self):
        super().__init__()
        self.selector = FormatSelector()

    def __getitem__(self):
        self.curidx += 1
        if self.curidx >= self.dataset_len:
            return None
        image_loc, annot_loc, image_format, annot_format, _ = self.dataset[self.curidx]
        annot_obj = self.process_annotation(annot_loc, annot_format, image_loc)
        return [image_loc, annot_obj]

    def process_annotation(self, annot_data, annot_format, image_loc):
        data = self.selector.select_format(annot_data, image_loc)
        return data
