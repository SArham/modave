from .base_generator import BaseGenerator
import cv2
import json
from src.annotation_parser.video_json_format import VideoJsonParser

"""
    [video_file, annotation_object]
"""


class VideoGenerator(BaseGenerator):

    def __init__(self):
        super().__init__()
        self.video_annotation_data = []
        self.process_data()

    def __getitem__(self):
        self.curidx += 1
        if self.curidx >= self.dataset_len:
            return None
        video_location, video_annotation, video_format, annotation_format, _ = self.dataset[self.curidx]
        annot_data = self.process_annotation(video_annotation, annotation_format)
        return [video_location, annot_data]

    @staticmethod
    def process_annotation(annot_data, annot_format):
        if annot_format == 'visiongenius':
            return VideoJsonParser(annot_data)