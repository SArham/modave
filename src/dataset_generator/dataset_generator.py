from .imageset_generator import ImageGenerator
from .video_generator import VideoGenerator


"""
    [location, annt_location, format, annt_format, is_image]
"""


class DatasetGenerator(object):

    def __init__(self, dataset, dataset_mapping, model_mapping):
        self.dataset = list(dataset)
        self.dataset_mapping = dataset_mapping
        self.model_mapping = model_mapping
        self.video_generator = VideoGenerator()
        self.image_generator = ImageGenerator()
        self.segregate_data()

    def segregate_data(self):
        for pair in self.dataset:
            if pair[-1]:
                self.image_generator.__add__(pair)
            else:
                self.video_generator.__add__(pair)
        self.video_generator.finalized = True
        self.image_generator.finalized = True
        self.image_generator.process_data()
        self.video_generator.process_data()
        self.get_total_dataset_size()

    def __getitem__(self):
        data = self.image_generator.__getitem__()
        if data is not None:
            return data, 1
        data = self.video_generator.__getitem__()
        if data is not None:
            return data, 2
        return None

    def __reset__(self):
        self.video_generator.__clear__()
        self.image_generator.__clear__()

    def get_total_dataset_size(self):
        if self.video_generator.dataset_len is not None and self.image_generator.dataset_len is not None:
            self.dataset_len = self.video_generator.dataset_len + self.image_generator.dataset_len
        if self.video_generator is None:
            self.dataset_len = self.image_generator.dataset_len
        if self.image_generator is None:
            self.dataset_len = self.video_generator.dataset_len

    def get(self):
        return self.__getitem__()