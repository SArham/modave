import os
import mimetypes

"""
    [location, annt_location, format, annt_format, is_image]
"""


class BaseGenerator(object):

    def __init__(self):
        self.dataset = []
        self.dataset_len = None
        self.finalized = False
        self.curidx = 0

    def __clear__(self):
        self.curidx = 0

    def __add__(self, pair):
        self.dataset.append(pair)

    def __len__(self):
        return len(self.dataset)

    def process_data(self):
        if not self.finalized:
            return
        to_remove = []
        to_explode = []

        for pair_idx, pair in enumerate(self.dataset):

            loc, anloc, fileformat, anformat, _ = pair
            isfile = self.is_file(loc)
            if isfile is None:
                to_remove.append(pair_idx)
            elif not isfile:
                to_remove.append(pair_idx)
                to_explode.append(pair_idx)

        for rm_idx in to_remove[::-1]:
            del self.dataset[rm_idx]

        for e_idx in to_explode:
            loc, anloc, fileformat, anformat, _ = self.dataset[e_idx]
            self.dataset.extend(self.get_files_from_dir(loc, anloc, fileformat, anformat))

        self.dataset_len = len(self.dataset)

    @staticmethod
    def is_file(location):
        if not os.path.exists(location):
            return None
        if os.path.isfile(location):
            mime = mimetypes.guess_type(location)[0]
            if 'image/' in mime:
                return True
            elif 'video/' in mime:
                return True
            else:
                return False
        if os.path.isdir(location):
            return False

    @staticmethod
    def get_files_from_dir(loc, anloc, formattypes, anformats):
        dataset_files = []
        annotation_files = []
        dformat = []
        anformat = []
        for root, directory, files in os.walk(loc):
            for file in files:
                if file.split('.')[-1] in formattypes:
                    dataset_files.append(os.path.join(root, file))
                    dformat.append(file.split('.')[-1])
        for root, directory, files in os.walk(anloc):
            for file in files:
                if file.split('.')[-1] in anformats:
                    annotation_files.append(os.path.join(root, file))
                    anformat.append(file.split('.')[-1])
        try:
            data = zip(dataset_files, annotation_files, dformat, anformat, [True for _ in range(len(dataset_files))])
            return data
        except Exception as exc:
            print(exc)
            ret_data = []
            fnames = [os.path.basename(path).split('.')[0] for path in dataset_files]
            anames = [os.path.basename(path).split('.')[0] for path in annotation_files]
            for fnidx, fname in enumerate(fnames):
                if fname not in anames:
                    continue
                annotation_index = anames.index(fname)
                if annotation_index == -1:
                    continue
                ret_data.append([dataset_files[fnidx], annotation_files[annotation_index],
                                 dformat[fnidx], anformat[annotation_index], True])
            return ret_data
