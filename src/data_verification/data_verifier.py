import os
import numpy as np
import math
from matplotlib import pyplot as plt

""" 
    IOU_Threshold: The percentage score of overlapping which signifies the boundary of true positive.

    True Positive: 
    False Positive:
    
    True Negative:
    False Negative:
    
"""


def average(mlist):
    if len(mlist) == 0:
        return 0
    return math.fsum(mlist) / len(mlist)


def match_labels(matched_bbox, slabels, plabels):
    if len(matched_bbox) == 0:
        return matched_bbox
    for source_id, prediction_data in matched_bbox.items():
        slabel = slabels[source_id]
        plabel = plabels[prediction_data['predicted']]
        if slabel != plabel:
            prediction_data['label'] = False
            continue
        prediction_data['label'] = True
    return matched_bbox


def calculate_metrics(matched_bboxes, unmatched_bboxes, true_negatives_bbox, source_annotation, predicted_boxes):
    no_source_bbox = len(source_annotation)
    no_predicted_bbox = len(predicted_boxes)

    true_positives = len(matched_bboxes.keys())
    false_positives = len(unmatched_bboxes.keys())
    false_negatives = no_source_bbox - true_positives
    true_negatives = len(true_negatives_bbox.keys())
    try:
        precision = true_positives / (true_positives + false_positives)
        recall = true_positives / (true_positives + false_negatives)
        accuracy = (true_positives + true_negatives) / \
                   (true_positives + true_negatives + false_negatives + false_positives)
        if recall == 0 and precision == 0:
            f1_score = 0
        else:
            f1_score = 2 * ((precision * recall) / (precision + recall))
    except Exception as exc:
        return 0.0, 0.0, 0.0, 0.0, [0, 0, 0, 0, 0, 0]
    return precision, recall, accuracy, f1_score, [true_positives, false_positives, true_negatives, false_negatives, no_source_bbox, no_predicted_bbox]


def splice_and_convert_to_np_array(arr):
    return np.asarray(list(zip(*list(zip(*arr))[:4]))), list(zip(*list(zip(*arr))[4:]))


class DataVerifier(object):

    def __init__(self, iou_threshold=0.60, iou_lower_bound=0.05):
        self.accuracy = []
        self.precision = []
        self.recall = []
        self.f1_score = []
        self.im_names = []
        self.base_info = []
        self.iou_threshold = iou_threshold
        self.iou_lower_bound = iou_lower_bound
        self.data = []
        self.metric_dataframe = None
        self.saving_folder = os.path.abspath(os.path.join(os.path.abspath(__file__), "..", "..", "..", "data"))

    def aoi(self, sources, preds):
        matches = {}
        unmatched = {}
        true_negatives = {}
        for pid, pbox in enumerate(preds):
            x1, y1, x2, y2 = pbox
            parea = (x2 - x1) * (y2 - y1)
            if sources.shape[0] == 0:
                break
            sx1, sy1, sx2, sy2 = sources[:, 0], sources[:, 1], sources[:, 2], sources[:, 3]
            sarea = (sx2 - sx1 + 1) * (sy2 - sy1 + 1)
            ox1, oy1, ox2, oy2 = np.maximum(x1, sx1), np.maximum(y1, sy1), np.minimum(x2, sx2), np.minimum(y2, sy2)
            ow, oh = np.maximum(0, ox2 - ox1 + 1), np.maximum(0, oy2 - oy1 + 1)
            oarea = ow * oh
            overlap = oarea / (sarea + parea - oarea)
            matched = np.argmax(overlap)
            if overlap[matched] >= self.iou_threshold:
                if matched not in matches.keys():
                    matches[matched] = {"predicted": pid, "area": overlap[matched]}
                if overlap[matched] > matches[matched]["area"]:
                    matches[matched] = {"predicted": pid, "area": overlap[matched]}
            elif overlap[matched] < self.iou_lower_bound:
                true_negatives[pid] = {"source": matched, "area": overlap[matched]}
            else:
                unmatched[pid] = {"source": matched, "area": overlap[matched]}
        return matches, unmatched, true_negatives

    def add_to_data(self, source, prediction):
        source_annotation = source.__getitem__()
        self.data.append([source_annotation, prediction])
        self.im_names.append(source.get_name())
        return source_annotation

    def compare_result(self, prediction, source):
        source_annotation = self.add_to_data(source, prediction)
        sboxes, slabels = splice_and_convert_to_np_array(source_annotation)
        pboxes, plabels = splice_and_convert_to_np_array(prediction)
        matched_bboxes, unmatched_bboxes, true_negatives = self.aoi(sboxes, pboxes)
        matched_bboxes = match_labels(matched_bboxes, slabels, plabels)
        return calculate_metrics(matched_bboxes, unmatched_bboxes, true_negatives, source_annotation, prediction)

    def __call__(self, prediction, source):
        """

        :param prediction: Result from the model passed through the model_verification_parser
        :param source: Result parser object from the dataset annotations
        :return: Nothing
        """
        precision, recall, accuracy, f1_score, base_info = self.compare_result(prediction, source)
        self.base_info.append(base_info)
        self.accuracy.append(accuracy)
        self.precision.append(precision)
        self.f1_score.append(f1_score)
        self.recall.append(recall)

    def fetch_metric(self, is_average=False):
        if is_average:
            return f"Avg. Precision: {average(self.precision)}, Avg. Recall: {average(self.recall)}, " \
                   f"Avg. Accuracy: {average(self.accuracy)}, Avg. F1 Score: {average(self.f1_score)}"
        return f"Precision: {self.precision[-1]}, Recall: {self.recall[-1]}, " \
               f"Accuracy: {self.accuracy[-1]}, F1 Score: {self.f1_score[-1]}"

    def create_verification_data(self):
        data = [['Index', "Names", "Precision", "Recall", "Accuracy", "F1 Score", "Source Boxes", "Predicted Boxes"]]
        for index, (names, precision, recall, accuracy, f1_score, base_data) in enumerate(
                zip(self.im_names, self.precision, self.recall, self.accuracy, self.f1_score, self.base_info)):
            data.append([index, names, precision, recall, accuracy, f1_score, base_data[-2], base_data[-1]])
        self.metric_dataframe = data

    def save_data(self):
        if self.metric_dataframe is None:
            self.create_verification_data()
        if not os.path.exists(self.saving_folder):
            os.makedirs(self.saving_folder)
        if os.path.exists(self.saving_folder):
            name = f"data_{len(os.listdir(self.saving_folder))}.csv"
            with open(f'{os.path.join(self.saving_folder, name)}', 'w') as data_file:
                for data in self.metric_dataframe:
                    row = ",".join([str(d) for d in data])
                    data_file.write(f"{row}\n")

    def plot_data(self):
        data = {
            "index": [i for i in range(len(self.precision))],
            "precision": self.precision,
            "accuracy": self.accuracy,
            "f1_score": self.f1_score,
            "recall": self.recall
        }
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(32, 22))

        fig.suptitle("Model Verification Metrics")
        ax1.plot('index', 'precision', data=data)
        ax1.set_ylabel("Precision")

        ax2.plot('index', 'accuracy', data=data)
        ax2.set_ylabel("Accuracy")

        ax3.plot('index', 'recall', data=data)
        ax3.set_ylabel("Recall")

        ax4.plot('index', 'f1_score', data=data)
        ax4.set_ylabel("F1 Score")

        plt.show()