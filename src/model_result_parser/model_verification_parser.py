import numpy as np

"""

    dim === (w, h, c) OR (w, h) The size of the original image. The annotation of the dataset is sized according to that.
    bbox.shape == (1, 4) or 4
    model_out_size === [w, h] The output size of the image from the model. The annotation is scaled according to that.
"""


def convert_bbox_to_list(bbox):
    """

    :param bbox: This could be a len 4 sized array or a len 5 array. A len 5 array includes a label.
    :return: A python list of len 4.
    """
    if isinstance(bbox, np.ndarray):
        if len(bbox.shape) == 1:
            return list(bbox)[:4]
        elif len(bbox.shape) > 1:
            return list(np.squeeze(bbox))[:4]
    return bbox[:4]


def assert_input(dim):
    assert dim is not None
    assert len(dim) >= 2


def assert_output_percentage(x1, y1, x2, y2, width, height):
    assert x1 < width and x2 <= width
    assert y1 < height and y2 <= height


def assert_output(x1, y1, x2, y2):
    assert x1 <= x2
    assert y1 <= y2


class ModelDataParser(object):

    def __init__(self, model_size=None):
        self.__name = None
        self.__conversion_dict = self.__set_conversion_table__()
        self.resultant_form = 'xyxy>1'
        self.conversion = None
        self.model_out_size = model_size

    def __call__(self, bboxes, labels, dimension=None):
        result = []
        for bbox, label in zip(bboxes, labels):
            bbox = self.conversion(bbox, dimension)
            result.append([*bbox, label])
        return result

    def set_name(self, name):
        self.__name = name

    def get_name(self):
        return self.__name

    def set_data_type(self, datatype):
        self.conversion = self.__conversion_dict[datatype]

    def choose_data_type(self, bbox, dim):
        for datatype, conversion in self.__conversion_dict.items():
            try:
                _ = conversion(bbox, dim)
                self.conversion = self.__conversion_dict[datatype]
            except Exception as exc:
                print(exc)

        if self.conversion is None:
            print("Conversion method cannot be set as the datatype you entered resulted in errors in all of "
                  "our converter. \nYou can add this to the model_verification_parser file and add it to the"
                  "conversion table.")

    def __set_conversion_table__(self):
        table = {'xywh<1': self.convert_xywh_percentage,
                 'xyxy>1': self.dont_convert,
                 'xywh>1': self.convert_xywh,
                 'xyxy<1': self.convert_xyxy_percentage,
                 'cxcywh>1': self.convert_cxcywh,
                 'cxcywh<1': self.convert_cxcywh_percentage}
        return table

    def rescale(self, bbox, dim):
        og_im_width, og_im_height = dim[:2]
        model_width, model_height = self.model_out_size[:2]
        ratio_w = og_im_width / model_width
        ratio_h = og_im_height / model_height
        x1, y1, x2, y2 = bbox
        return [int(x1 * ratio_w), int(y1 * ratio_h), int(x2 * ratio_w), int(y2 * ratio_h)]

    @staticmethod
    def convert_xywh_percentage(bbox, dim=None):
        assert_input(dim)
        bbox = convert_bbox_to_list(bbox)
        x, y, w, h = bbox
        width, height = dim[:2]
        x1 = x * width
        y1 = y * height
        x2 = x1 + (w * width)
        y2 = y1 + (h * height)
        assert_output_percentage(x1, y1, x2, y2, width, height)
        return [int(x1), int(y1), int(x2), int(y2)]

    def dont_convert(self, bbox, dim=None):
        assert_input(dim)
        bbox = convert_bbox_to_list(bbox)
        bbox = self.rescale(bbox, dim)
        assert_output_percentage(*bbox, dim[0], dim[1])
        return bbox

    def convert_xywh(self, bbox, dim=None):
        assert_input(dim)
        bbox = convert_bbox_to_list(bbox)
        x, y, w, h = bbox
        x2 = x + w
        y2 = y + h
        bbox = self.rescale([x, y, x2, y2], dim)
        assert_output(*bbox)
        return bbox

    @staticmethod
    def convert_xyxy_percentage(bbox, dim=None):
        assert_input(dim)
        bbox = convert_bbox_to_list(bbox)
        width, height = dim[:2]
        x1, y1, x2, y2 = bbox
        x1, x2 = x1 * width, x2 * width
        y1, y2 = y1 * height, y2 * height
        assert_output_percentage(x1, y1, x2, y2, width, height)
        return [int(x1), int(y1), int(x2), int(y2)]

    @staticmethod
    def convert_cxcywh_percentage(bbox, dim=None):
        assert_input(dim)
        bbox = convert_bbox_to_list(bbox)
        width, height = dim[:2]
        cx, cy, w, h = bbox
        w2 = w / 2
        h2 = h / 2
        x1 = (cx - w2) * width
        x2 = (cx + w2) * width
        y1 = (cy - h2) * height
        y2 = (cy + h2) * height
        assert_output_percentage(x1, y1, x2, y2, width, height)
        return [int(x1), int(y1), int(x2), int(y2)]

    def convert_cxcywh(self, bbox, dim=None):
        assert_input(dim)
        bbox = convert_bbox_to_list(bbox)
        cx, cy, w, h = bbox
        w2 = w / 2
        h2 = h / 2
        x1 = (cx - w2)
        x2 = (cx + w2)
        y1 = (cy - h2)
        y2 = (cy + h2)
        bbox = self.rescale([x1, y1, x2, y2], dim)
        assert_output(*bbox)
        return bbox
