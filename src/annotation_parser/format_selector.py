import os
from .image_yolo_format import ImageYoloFormat
from .image_coco_format import ImageCocoFormat
from .image_voc_format import ImageVOCFormat


class FormatSelector(object):

    def __init__(self):
        pass

    @staticmethod
    def try_voc(annotation_location):
        try:
            formatter = ImageVOCFormat(annotation_location)
            return formatter
        except:
            return None

    @staticmethod
    def try_coco(annotation_location):
        try:
            formatter = ImageCocoFormat(annotation_location)
            return formatter
        except:
            return None

    @staticmethod
    def try_yolo(annotation_location, image_loc):
        try:
            formatter = ImageYoloFormat(annotation_location, image_loc)
            return formatter
        except:
            return None

    def select_format(self, annotation_location,image_loc=None):
        basename = os.path.basename(annotation_location)
        formatter = None
        if '.xml' in basename:
            formatter = self.try_voc(annotation_location)

        if '.txt' in basename:
            formatter = self.try_yolo(annotation_location, image_loc)

        if '.json' in basename:
            formatter = self.try_coco(annotation_location)

        return formatter
