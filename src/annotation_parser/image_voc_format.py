import os
from xml.etree import ElementTree as et

from .base_format import BaseFormat


class ImageVOCFormat(BaseFormat):
    base_categories = {}

    def __init__(self, annotation_location):
        super().__init__(annotation_location)
        self.data = self.get_pascal_voc_data(annotation_location)

    def normalize(self, x1, y1, x2, y2, width=None, height=None):
        if x1 >= x2:
            if x1 + x2 < 1.0:
                x1 = int(x1 * self.width)
                x2 = int(x2 + self.width)
        if y1 >= y2:
            if y1 + y2 < 1.0:
                y1 = int(y1 * self.height)
                y2 = int(y2 * self.height)
        return int(x1), int(y1), int(x2), int(y2)

    def get_pascal_voc_data(self, annotation_file):
        annotation = et.parse(annotation_file).getroot()
        self.width = int(annotation.find('size').find('width').text)
        self.height = int(annotation.find('size').find('height').text)
        boxes = []
        for obj in annotation.getiterator('object'):
            name = obj.find('name').text
            bndbox = obj.find('bndbox')
            x1, y1, x2, y2 = int(float(bndbox.find('xmin').text)), \
                             int(float(bndbox.find('ymin').text)), \
                             int(float(bndbox.find('xmax').text)), \
                             int(float(bndbox.find('ymax').text))
            self.raw_data.append([x1, y1, x2, y2])
            x1, y1, x2, y2 = self.normalize(x1, y1, x2, y2)
            box = [x1, y1, x2, y2, name]
            boxes.append(box)
        return boxes

