import os
import json


class VideoJsonParser(object):

    def __init__(self, annotation):
        if type(annotation, str):
            try:
                self.annotation = json.loads(annotation)
            except:
                if os.path.exists(annotation) and os.path.isfile(annotation):
                    self.annotation = json.load(open(annotation, 'r'))
        elif type(annotation, dict):
            self.annotation = annotation

        self.keys = [key for key in self.annotation.key() if key.isnumeric()]
        self.data = self.parse_annotation_file()
        self.len = len(self.data)
        self.curidx = 0

    @staticmethod
    def normalize_value(value, dimension_max):
        if 0 < value < 1.0:
            return int(value * dimension_max)
        if value < 0:
            return 0
        if dimension_max < value:
            return int(dimension_max)
        return int(value)

    def normalize(self, x, y, w, h, frame_width, frame_height):
        x = self.normalize_value(x, frame_width)
        y = self.normalize_value(y, frame_height)
        w = self.normalize_value(w, frame_width)
        h = self.normalize_value(h, frame_height)
        assert x + w < frame_width
        assert y + h < frame_height
        return x, y, w, h

    @staticmethod
    def standardize_annotation(bbox):
        x, y, w, h = bbox
        return int(x), int(y), int(x+w), int(y+h)

    def parse_annotation_file(self):
        frame_width = self.annotation['frame_x']
        frame_height = self.annotation['frame_y']
        information = []
        for frame_number, value in self.annotation.items():
            if not frame_number.isnumeric():
                continue
            data = value['data']
            frame_data = []
            for info in data:
                h = info['h']
                w = info['w']
                x = info['x']
                y = info['y']
                label = info['label']
                bbox = self.normalize(x, y, w, h, frame_width, frame_height)
                x1, y1, x2, y2 = self.standardize_annotation(bbox)
                frame_data.append([x1, y1, x2, y2, label])
            information.append([frame_number, frame_data])
        return information

    def __getitem__(self):
        self.curidx += 1
        if self.curidx >= self.len:
            self.curidx = 0
        return self.data[self.curidx-1]
