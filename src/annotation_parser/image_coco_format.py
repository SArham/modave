import os
import json
from .base_format import BaseFormat


class ImageCocoFormat(BaseFormat):
    """
        {
        "annotation" : [{}, {}],
        "categories" : {},
        "images" : {}
        }
    """
    base_categories = {}

    def __init__(self, annotation_location=None):
        super().__init__(annotation_location)
        self.categories = None
        self.data = self.process_data_from_file(annotation_location)

    @staticmethod
    def normalize_value(val, dimension_max):
        if isinstance(val, float):
            if val <= 1.0:
                val = int(val * dimension_max)
                return val
            return int(val)
        if isinstance(val, int):
            if val <= 0:
                return 0
            return val

    def normalize(self, x, y, w, h, height=None, width=None):

        if self.height is not None and self.width is not None:
            height = self.height
            width = self.width

        x = self.normalize_value(x, width)
        w = self.normalize_value(w, width)
        y = self.normalize_value(y, height)
        h = self.normalize_value(h, height)
        assert x + w < self.width
        assert y + h < self.height
        return x, y, x + w, y + h

    @staticmethod
    def standardize_annotation(bbox):
        """
        :param bbox: x, y, w, h
        :return: x1, y1, x2, y2
        """
        x, y, w, h = bbox
        x2 = x + w
        y2 = y + h
        return int(x), int(y), int(x2), int(y2)

    def process_coco_complete_data(self, cocodata, annotation_location):
        im_name = os.path.basename(annotation_location).split('.')[0]
        images = cocodata['images']
        annotations = cocodata['annotations']
        self.categories = cocodata['categories']
        annot_data = []
        for imd in images:
            imdname = imd['file_name'].split('.')[0]
            if imdname != im_name:
                continue
            imid = imd['id']
            self.height = int(imd['height'])
            self.width = int(imd['width'])
            for annot in annotations:
                if annot['image_id'] != imid:
                    continue
                x, y, w, h = annot['bbox']
                x, y, w, h = self.normalize(x, y, w, h)
                x1, y1, x2, y2 = self.standardize_annotation([x, y, w, h])
                annot_data.append([x1, y1, x2, y2, annot['category_id']])
        return annot_data

    def process_data_from_file(self, annotation_location):
        if annotation_location is None:
            return None
        annotation_data = json.load(open(annotation_location, 'r'))
        keys = annotation_data.keys()
        annotation = annotation_data['annotation']
        annot_data = []
        if 'image' not in keys:
            for annot in annotation:
                x, y, w, h = annot['bbox']
                self.raw_data.append([x, y, w, h])
                x2, y2 = x + w, y + h
                cat_id = annot['category_id']
                annotation_data.append([x, y, x2, y2, cat_id])
            return annot_data
        return self.process_coco_complete_data(annotation_data, annotation_location)
