import os


class BaseFormat(object):

    def __init__(self, annotation_location):
        """
        Use this class as the parent class for any other type of annotation format.
        Fill these properties with relevant data.
        width, height = Dimensions of the image
        raw_data = Raw information extractec from the annotation
        data = Processed, normalized, standardized information.
        name = Name of the annotaiton/image.
        """
        self.width = None
        self.height = None
        self.raw_data = []
        self.data = None
        self.name = os.path.basename(annotation_location).split(".")[0]
        self.collection_name = None
        for val in annotation_location.split('/')[-4:]:
            if "." in val:
                continue
            if val in ['data', 'Annotation', "JPEGImages", "Images", "labels", "Dataset"]:
                continue
            self.collection_name = val

    def __getitem__(self):
        return self.data

    def rescale(self, bbox, dim):
        og_im_width, og_im_height = self.width, self.height
        model_width, model_height = dim[:2]
        ratio_w = model_width / og_im_width
        ratio_h = model_height / og_im_height
        x1, y1, x2, y2, _ = bbox
        return [int(x1 * ratio_w), int(y1 * ratio_h), int(x2 * ratio_w), int(y2 * ratio_h)]

    def resize(self, dim):
        resized_bbox = []
        for bbox in self.data:
            resized_bbox.append(self.rescale(bbox, dim))
        return resized_bbox

    def get_name(self):
        return self.name