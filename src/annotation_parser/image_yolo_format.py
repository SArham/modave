import os
import cv2
from .base_format import BaseFormat


class ImageYoloFormat(BaseFormat):

    base_categories = {}

    def __init__(self, annotation_location, image_loc):
        super().__init__(annotation_location)
        self.height, self.width = cv2.imread(image_loc).shape[:2]
        self.data = self.process_data(annotation_location)

    def standardize_annotation(self, bbox):
        x, y, w, h = bbox
        w2 = w/2
        h2 = h/2
        x1 = x - w2
        x2 = x + w2
        y1 = y - h2
        y2 = y + h2
        return max(0, int(x1 * self.width)), max(0, int(y1 * self.height)), int(x2 * self.width), int(y2 * self.height)

    @staticmethod
    def normalize(x, y, w, h):
        assert w > 0.
        assert h > 0.
        return x, y, w, h

    def process_data(self, annotation_location):
        annotation_data = []
        with open(annotation_location, 'r') as annot_data:
            for line in annot_data.readlines():
                data = line.replace('\n', '').split(' ')
                object_class, x, y, w, h = data
                bbox = self.normalize(float(x), float(y), float(w), float(h))
                self.raw_data.append(bbox)
                x1, y1, x2, y2 = self.standardize_annotation(bbox)
                annotation_data.append([x1, y1, x2, y2, int(object_class)])
        return annotation_data
