import os
import json
from natsort import natsorted
from argparse import ArgumentParser

""" 
    INPUT STRUCT::
    JSON structure:
        dataset:
            imageset: [[location, [img/folder], [annt/folder], [image_format(s)], [annotation_format(s)]]] 
            
            videoset: [[location, [vid/folder], [annt/folder],  [video_format(s)], [annotation_format(s)]]]
            
            mapping:
            
        
        model:
            mapping:
            
            
    OUTPUT STRUCT::
    dataset:
        
        """


def get_data_annt(location, annotation_location, format, annt_format, is_image):
    location = os.path.abspath(location)
    annt_location = os.path.abspath(annotation_location) if os.path.isfile(annotation_location) else None
    if annt_location is not None:
        return [location, annt_location, format, annt_format, is_image]
    return None


def create_data_annotation_set(imageset, annotationset, img_frmts, ant_frmts, is_image):
    im_ant_set = []
    for img, annt in zip(imageset, annotationset):
        img_basename = os.path.basename(img)
        ant_basename = os.path.basename(annt)
        img_name, img_frmt = img_basename.split('.')[0], img_basename.split('.')[-1]
        ant_name, ant_frmt = ant_basename.split('.')[0], ant_basename.split('.')[-1]
        if img_frmt not in img_frmts:
            continue
        if ant_frmt not in ant_frmts:
            continue
        if img_name != ant_name:
            continue
        im_ant_set.append([img, annt, img_frmt, ant_frmt, is_image])
    return im_ant_set


class InputArgumentParser(object):

    def __init__(self, json_data=None):
        self.json_data = json_data
        process = self.process_input_arg()

        self.json_structure = {
            'dataset': ['imageset', 'videoset', 'maping'],
            'model': ['mapping']
        }
        self.to_ignore = ['classes.txt']
        self.json_structure_main_keys = len(self.json_structure.keys())
        self.dataset = []
        self.imageset = None
        self.videoset = None
        self.mapping, self.model_mapping = None, None
        if not process:
            self.argparser = ArgumentParser("Model Verification Engine")
            self.argparser.add_argument("-f", '--file', dest='file',
                                        help="Location of the json file with the dataset information")
            self.arguments = self.argparser.parse_args()
            fileloc = self.arguments.file
            self.process_input_file(fileloc)

    def process_input_arg(self):
        if type(self.json_data) is None:
            return False
        elif type(self.json_data) is str:
            self.process_input_file(self.json_data)
            return True
        elif type(self.json_data) is dict:
            if len(self.json_data.keys()) == self.json_structure_main_keys:
                return True
        return False

    def process_input_file(self, fileloc):
        if not os.path.exists(fileloc):
            return "Path does not exist"
        if not os.path.isfile(fileloc):
            return "Path is not a file"
        self.json_data = json.load(open(fileloc, 'r'))

    def process_dict_data(self):
        self.imageset = self.json_data['dataset']['imageset']
        self.videoset = self.json_data['dataset']['videoset']
        self.mapping = self.json_data['dataset']['mapping']
        self.model_mapping = self.json_data['model']['mapping']


    def get_files(self, folder, format):
        dir_files = []
        for root, dir, files in os.walk(folder):
            for file in files:
                if file in self.to_ignore:
                    continue
                if file.split('.')[-1] in format:
                    cfile = os.path.join(root, file)
                    dir_files.append(cfile)
        return list(natsorted(dir_files))

    def extract_data_annt_from_folder(self, image_folder, annt_folder, image_formats, annt_formats):
        images = self.get_files(image_folder, image_formats)
        annots = self.get_files(annt_folder, annt_formats)
        return images, annots, image_formats, annt_formats

    def process_data_folder(self, img_loc, img_frmt, ant_loc, ant_frmt, is_image):
        if not os.path.isdir(ant_loc):
            return
        dataset = create_data_annotation_set(
            *self.extract_data_annt_from_folder(img_loc, ant_loc, img_frmt, ant_frmt), is_image)
        return dataset

    def process_dataset(self, location, format, annotation_location, annt_format, is_image):
        if os.path.isfile(location):
            data = get_data_annt(location, annotation_location, format, annt_format, is_image)
            if data is None:
                return
            self.dataset.append(data)

        elif os.path.isdir(location):
            data = self.process_data_folder(location, format, annotation_location, annt_format, is_image)
            if data is None:
                return
            for datapoint in data:
                self.dataset.append(datapoint)

    def process_total_dataset(self):
        for imset in self.imageset:
            if not imset:
                continue
            location, is_image, annotation_location, format, annt_format = imset
            self.process_dataset(location, format, annotation_location, annt_format, True)

        for vidset in self.videoset:
            if not vidset:
                continue
            location, is_image, annotation_location, format, annt_format = vidset
            self.process_dataset(location, format, annotation_location, annt_format, False)

    def process_mapping(self, is_data=True):
        dataset_mapping = {}
        if is_data:
            cur_mapping = self.mapping
        else:
            cur_mapping = self.model_mapping
        if type(cur_mapping) is list:
            for mapping in cur_mapping:
                key, value = mapping
                dataset_mapping[key] = value
        elif type(cur_mapping) is dict:
            for key, value in self.mapping.items():
                dataset_mapping[key] = value
        elif type(cur_mapping) is str:
            if os.path.exists(cur_mapping) and os.path.isfile(cur_mapping):
                if '.json' in cur_mapping:
                    dataset_mapping = json.load(open(cur_mapping, 'r'))
                elif '.csv' in cur_mapping:
                    with open(cur_mapping, 'r') as fdata:
                        for line in fdata.readlines():
                            if ',' in line:
                                line_data = line.replace('\n', '').replace(' ', '').split(',')
                                dataset_mapping[line_data[0]] = line_data[1]
        if is_data:
            self.mapping = dataset_mapping
        else:
            self.model_mapping = dataset_mapping

    def extract_dataset(self):
        self.process_dict_data()
        self.process_total_dataset()
        self.process_mapping(is_data=True)
        self.process_mapping(is_data=False)

    def get_dataset(self):
        return self.dataset, self.mapping, self.model_mapping