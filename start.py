import os
import sys
import api.main as generator
sys.path.append("/home/trafficgenius/TrafficGenius/darknet")


if __name__ == '__main__':
    dataset_generator = generator.get_dataset_generator(inputdata="/home/trafficgenius/TrafficGenius/modave/data.json")
    model_data_converter = generator.get_model_data_converter(model_size=[800, 640])
    model_data_converter.set_name("yolo")
    data_verifier = generator.get_data_verifier()

    for i in range(dataset_generator.dataset_len - 1):
        result = dataset_generator.get()
        instance_info, medium = result
        source_loc, annotation_obj = instance_info
        print(source_loc, annotation_obj.__getitem__())



